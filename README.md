# firestore-explorer-cmd

A simple Nodejs Firestore explorer with Firebase Admin SDK.

## Getting started

SDK CLIENT REFERENCE https://googleapis.dev/nodejs/firestore/latest/index.html

Features:

 - Querying, integrated with Firestore
 - Exporting rows as `.json`
