const firebase = require('firebase-admin');
const readline = require('readline');
const fs = require('fs');
const write = require('write');
const scripts = require('./line-scripts.js');
const serviceAccounts = require('./service-accounts.js');

/*
 *  General utils
 */
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
rl.on('close', () => {
  process.exit(0);
});

let cmd = []; // For line commands
const input = async (mess = "") => {
  if (cmd.length) {
    const output = cmd[0];
    cmd.splice(0, 1);
    console.log(`${mess} > \x1b[33mCommand\x1b[0m: ${output}, \x1b[33mremain\x1b[0m:`, cmd);
    return output;
  }
  return await new Promise(resolve => {
    rl.question(mess, resolve);
  });
};

const sysLog = (mess = "", type = "") => {
  let typeStr = "\x1b[33m[Sys]\x1b[0m";
  if (type === "Err") typeStr = "\x1b[31m[Err]\x1b[0m";
  console.log(`${typeStr} ${mess}`);
};

// Main script
const run = async () => {
  sysLog("Command modes:");
  [
    "1. Saved scripts",
    "2. Enable line sequence commands",
    "3. (default)read-one-by-one",
  ].map(v => console.log(v));
  let mode = parseInt(await input("Set mode: "));
  let line;
  if (!mode) mode = 3;
  switch (mode) {
    case 1:
      const scriptIndex = 0;
      Object.keys(scripts).map(v => console.log(`${scriptIndex+1}. [${v}] ${scripts[v]}`));
      const script = parseInt(await input("Select script: "));
      if (script && Object.keys(scripts)[script-1]) line = scripts[Object.keys(scripts)[script-1]];
    case 2:
      if (mode === 2) line = await input("Line sequence commands (Separated by ;): \n");
      cmd = line.split(";").filter(v => v);
      break;
    default:
      console.log("Commands:read-one-by-one");
  }

  // Service account list
  sysLog("Project(s):");
  for (let i in serviceAccounts) {
    console.log(`${parseInt(i)+1}. ${serviceAccounts[i].project_id}`);
  }
  const serviceAccount = serviceAccounts[parseInt(await input("Pick project: ") - 1)];
  if (!serviceAccount) return end("No project selected", true);

  sysLog("Init connection");
  const admin = firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
  });
  const db = admin.firestore();
  if (!db) return end("Db not found", true);

  sysLog("Collections:");
  const collectionRefs = await db.listCollections();
  const collections = [];
  for (let i = 0; i < collectionRefs.length; i++) {
    collections.push(collectionRefs[i].id);
    console.log(`${i+1}. ${collectionRefs[i].id}`);
  }

  const collection = await input("Pick collection: ");
  if (collections.indexOf(collection) < 0) return end("Invalid collection", true);

  if (["1", 1, "true", true].indexOf((await input(`(Need latest schema?, \x1b[32m1/true\x1b[0m or (default)\x1b[31m0/false\x1b[0m): `))) > -1) {
    // Fetch latest doc
    sysLog("Collection schema:");
    const schema = ((await db.collection(collection).orderBy("createDatetime").limit(1).get())._docs() || [{}])[0]._fieldsProto;
    Object.keys(schema).map(v => schema[v] = schema[v].valueType);
    console.log(schema);
  }

  sysLog("Get document(s) by:");
  const getBys = [
    "1. Documents",
    "2. uuid",
    "3. Conditions",
  ];
  getBys.map(v => console.log(v));
  const getBy = parseInt(await input("(Pick a number): "));
  sysLog(`Get with ${getBys[getBy-1] || "\`N/A\`"}`);

  const extraFilters = async docRef => {
    let filter;
    let filters = ["orderBy", "limit"];
    while (filters.length) {
      filter = filters.indexOf(await input(`(Need filter(s)?, ${filters.join(" or ")} or (default)\x1b[31m0/false\x1b[0m): `));
      // Add filter
      switch (filters[filter]) {
        case "orderBy":
          docRef = docRef.orderBy(
            await input("(Field): "),
            (await input("(asc or desc): ")) === "desc" ? "asc" : "desc" // FIXME seems SDK reverted those keywords?
          );
          break;
        case "limit":
          docRef = docRef.limit(parseInt(await input("Limit numbers: ")));
          break;
        default:
          filters = [];
      }
      filters.splice(filter, 1);
    }

    return docRef;
  };

  let query, doc;
  console.log("...loading...");
  switch (getBy) {
    case 1:
      doc = db.collection(collection);
      doc = await extraFilters(doc);
      query = await doc.get();
      break;
    case 2:
      const uuid = await input("(Document uuid): ");
      doc = db.collection(collection).doc(uuid);
      doc = await doc.get();
      query = {
        _size: doc._fieldsProto ? 1 : 0,
        _docs: () => [doc],
      };
      break;
    case 3:
      let setConditions = true;
      doc = db.collection(collection);
      let field, op, values;
      while (setConditions) {
        sysLog("Input conditions:");
        field = await input("(Field): ");
        op = await input("(Operator, (default)== / != / > / >= / < / <= / in / not-in / array-contains / array-contains-any): ");
        if (!op) op = "==";
        switch (op) {
          case "in":
          case "not-in":
          case "array-contains-any":
            values = [];
            let valueArray = true;
            while(valueArray) {
              values.push(await input(`(Value${values.length+1}): `));
              if (values[values.length - 1] === "null" && ["1", 1, "true", true, ""].indexOf((await input(`(Parse to null object?, (default)\x1b[32m1/true\x1b[0m or \x1b[31m0/false\x1b[0m): `))) > -1) values[values.length - 1] = null; // Parse a null
              valueArray = ["1", 1, "true", true].indexOf((await input(`(More values?, \x1b[32m1/true\x1b[0m or \x1b[31m0/false\x1b[0m): `))) > -1;
            }
            break;
          default:
            values = await input("(Value): ");
            if (values === "null" && ["1", 1, "true", true, ""].indexOf((await input(`(Parse to null object?, (default)\x1b[32m1/true\x1b[0m or \x1b[31m0/false\x1b[0m): `))) > -1) values = null; // Parse a null
        }
        doc = doc.where(field, op, values);
        setConditions = ["1", 1, "true", true].indexOf((await input(`(More conditions?, \x1b[32m1/true\x1b[0m or (default)\x1b[31m0/false\x1b[0m): `))) > -1;
      }
      query = await doc.get();
      break;
    default:
      sysLog("Action not match");
  }
  console.log("...loading...");

  if (!query) return end();
  if (!query._size) return end("No docs found");
  sysLog(`${query._size} file(s) read`);

  sysLog("Action(s) to perform: ");
  const actions = [
    "1. (default)List data",
    "2. Export to \`.json\`",
    "0. Exit script",
  ];
  actions.map(v => console.log(v));
  let action = parseInt(await input("(Pick a number): "));
  if (isNaN(action)) action = 1;
  sysLog(`Do: ${actions[action-1] || "\`N/A\`"}`);

  let docs = (await query._docs()).map(v => {
    const data = v.data();
    data.id = v.id;
    return data;
  });

  switch (action) {
    case 1:
      sysLog(`Read data:`);

      // Paginations
      let perPage = 20;
      let page = 1;
      let pages, start, last, allDocs;
      let pagingLoop = true;
      while (pagingLoop) {
        if (docs.length > perPage) {
          // Get total pages
          pages = Math.ceil(docs.length / perPage);
          page = parseInt(await input(`(Page number, page (default)\x1b[32m1\x1b[0m to \x1b[32m${pages}\x1b[0m): `));
          if (page < 1) page = 1;
          if (page > pages) page = pages;

          start = perPage * (page - 1);
          last = perPage * page;

          allDocs = [].concat(docs);
          docs.splice(start, perPage); // Set targeted docs
        } else {
          pagingLoop = false;
        }

        for(const i in docs) {
          const doc = docs[i];
          console.log(`Reference: ${collection}.${doc.id}`, doc);
        }
        if (allDocs) docs = [].concat(allDocs); // Reset all docs

        if (pagingLoop) pagingLoop = ["1", 1, "true", true].indexOf((await input(`(Keep reading?, \x1b[32m1/true\x1b[0m or (default)\x1b[31m0/false\x1b[0m): `))) > -1;
      }

      break;
    case 2:
      const date = new Date();
      const outputPath = `${__dirname}/${serviceAccount.project_id}.${collection}.export.${date.getDate()}-${date.getMonth()}-${date.getFullYear()}.json`;
      const content = JSON.stringify(docs);
      /* fs is not work
      fs.writeFile(outputPath, content, {
        encoding: "utf8",
        mode: 0o666,
        flag: "a+",
      }, err => {
        if (err) {
          sysLog("File write error:", true);
          console.log(err);
        }
      });
      */
      await write(outputPath, content);
      sysLog(`File created at ${outputPath}`);
      break;
    case 0:
      sysLog("Bye!");
      break;
    default:
      sysLog("Action not match");
  }

  end();
};
const end = (mess = "", isErr = false) => {
  if (mess) sysLog(mess, isErr ? "Err" : "");

  sysLog("Script finished");
  rl.close(); // Need last
};

// Run main script
run();
